# Pacman Update Notifier
Simple update notification script for pacman.

Uses DBus to send update notifications.

## Setup
Clone this repositiory.

Run `bundle install` inside the clone directory.

Then run `bundle exec ruby ./pacman-update-notify.rb`.
